import java.io.File
import java.util.Scanner

val scanner = Scanner(System.`in`)
var numTries = 0
var answers: MutableList<String> = mutableListOf()
var restartGame = false
var userWord = ""
val secretWords = File("src/main/kotlin/files/wordsFile").readText().trim().split(",")
var won = false
var lang = true //ESP=true - ENG=false

lateinit var secretWord: String

//----COLORES-----------------------------------------------
val green = "\u001B[42m"
val red = "\u001B[41m"
val gray = "\u001B[47m"
val yellow = "\u001B[43m"
val noColor = "\u001b[0m"
//----------------------------------------------------------
/**
 * WORDLE MAIN CODE
 * @author Albert Darchiev
 * @version 2.0 26-11-2022
 * @param main
 */
fun main() {
    //-----------------------BUCLE JUEGO------------------------
    var selectMenu = 0
    if(lang)println("1.JUGAR\n2.CAMBIAR IDIOMA/INGLES\n3.MOSTRAR HISTORIAL\n4.SALIR")
    else println("1.PLAY\n2.CHANGE LANGUAGE/SPANISH\n3.SHOW HISTORIAL\n4.EXIT")
        selectMenu = scanner.nextInt()
        when(selectMenu){
            1-> mainGame()
            2-> changeLang()
            3-> showGameHistoric()
            4-> println("BYE")
        }
    }


/**
 * MAIN GAME - Contiene la logica principal del juego Wordle
 */
fun mainGame(){
    if(lang) {
        println(
            "--------------------------------------------WORDLE--------------------------------------------\n" +
                    "---------------------------------------- NORMAS JUEGO ----------------------------------------\n" +
                    "Introduce palabras de 5 letras, tienes 6 intentos para adivinar la palabra secreta.\n" +
                    " -El fondo GRIS representa las letras que no estan en la palabra secreta\n" +
                    " -El fondo AMARILLO representa las letras que se encuentran en la palabra, pero estan en la posición incorrecta\n" +
                    " -El fondo VERDE representa las letras que se encuentran en la posición correcta\n"
        )
    }
    else{
        println("--------------------------------------------WORDLE--------------------------------------------\n" +
                "----------------------------------------- GAME RULES -----------------------------------------\n" +
                "Introduce a 5 letters word, you will have 6 tries to guess the secret word.\n" +
                " -The GRAY background represents the letters that are not in the secret word\n" +
                " -The YELLOW background represents the letters that are in the word, but are in the wrong position\n" +
                " -The GREEN background represents the letters that are in the correct position\n" )
    }
    do {
        restartGame = false
        numTries = 0
        userWord =""
        answers= mutableListOf()
        secretWord = secretWords.random()
        println(secretWord)

        if (lang)println(green + "Introduce una palabra de 5 letras:" + noColor)
        else println(green + "Introduce a 5 letters word:" + noColor)
        do {
            userWord = scanner.next().uppercase()
            if (userWord.length != 5) {
                if(lang)println("LA PALABRA TIENE QUE CONTENER 5 LETRAS!")
                else println("WORD MUST HAVE 5 LETTERS!")
                numTries--
            } else {
                println(checkLetters(secretWord, userWord))
                printHistorial(checkLetters(secretWord, userWord))
            }
            if (userWord == secretWord) {
                won = true
                if(lang)println("\n"+ green + "¡¡¡HAS GANADO!!!" + noColor)
                else println("\n"+ green + "¡¡¡YOU WON!!!" + noColor)
                break
            }
            numTries++
        } while (numTries < 6)
        if (numTries == 6 && userWord != secretWord){
            if(lang)println(red +"HAS PERDIDO..." + noColor)
            else println(red +"YOU LOST..." + noColor)
        }
        if (lang)println("PALABRA SECRETA: $secretWord \n")
        else println("SECRET WORD: $secretWord \n")

        saveGameStats()


        if (lang)println("¿Quieres jugar otra partida?    SI | NO") // Volver a comenzar
        else println("Do you want to play another game    YES | NO")
        var userInp = scanner.next().toUpperCase()
        restartGame = userInp == "SI" || userInp == "YES"
    } while (restartGame)
main()
}

/**
 * CAMBIAR IDIOMA - DEPENDIENDO DEL IDIOMA ACTUAL LO CAMBIA POR OTRO (INGLES O CASATELLANO)
 */
fun changeLang(){
    if(lang){
        println("GAME LANGUAGE HAVE BEEN CHANGED")
        lang = false
    }
    else{
        println("EL IDIOMA DEL JUEGO HA SIDO CAMBIADO")
        lang = true
    }
    main()
}

/**
 * GUARDAR ESTADISTICAS PARTIDA - Guarda en un archivo de texto un resumen de la partida (Usuario, Intentos y palabra secreta)
 */
fun saveGameStats(){
    val file = File("src/main/kotlin/files/historialFile")
    if (lang)println("Introduce tu nombre de usuario: ")
    else println("Introduce your user name: ")
    val username = scanner.next()
    file.appendText("$username.$numTries.$secretWord\n")
}

/**
 * IMPRIMIR HISTORIAL - Imprime por consola el historial completo de partidas jugadas que se guardan en files>historialFile
 */
fun showGameHistoric(){
    val file = File("src/main/kotlin/files/historialFile")

    var history = file.readText().split("\n").toMutableList()
    history.removeAt(history.size-1)
    println(history)
    if (lang) {
        for (userStat in history) {
            print(yellow)
            val stat = userStat.split(".")
            println("USUARIO: ${stat[0]}")
            if (stat[1]!="6")println("HA GANADO con ${stat[1]+1} intentos")
            else println("HA PERDIDO")
            println("PALABRA SECRETA: ${stat[2]}\n")
            print(noColor)
        } }
    else {
        for (userStat in history) {
            print(yellow)
            val stat = userStat.split(".")
            println("USER: ${stat[0]}")
            if (stat[1]!="6")println("HAVE WON with ${stat[1]+1} tries")
            else println("HAVE LOST")
            println("SECRET WORD: ${stat[2]}\n")
            print(noColor)
        }
    }
main()
}

/**
 * CAMBIAR COLOR FONDO - Comprueva las letras 1 por 1 de la palabra secreta y la insertada, y cambia el color del fondo dependiendo de la posicion de las letras.
 * <br/>v2 - Se crea una lista mutable de las letras de la PALABRA SECRETA y una para las letras de la RESPUESTA del usuario.
 * <br/>        1r BUCLE - Si la posición de las letra entre la palabra secreta e introducida coincide se elimina esa letra de la lista PALABRA SECRETA, y se añade a la lista ANSWER
 * <br/>        2o BUCLE - Si no coincide la posición, pero se encuentra en la lista se elimina también y se añade esa letra a la lista ANSWER.
 *                        - En caso de que esa letra no se encuentre en la lista SecretWordList se cambia a gris el color de esa letra en la lista ANSWER
 * <br/>        (De esta forma no muestra letras naranjas de más)
 * <br/> Una vez finalizados los bucles se añade la lista "ANSWER" a "ANSWERS", donde se gurdan todas las respuestas anteriores y se imprime
 * @param secretWord - Variable que contiene la palabra secreta en String
 * @param userWord - Variable String introducida por el usuario
 */
fun checkLetters(secretWord: String, userWord:String): MutableList<String> {
    var secretWordList = secretWord.split("").drop(1).dropLast(1).toMutableList()
    var answer = mutableListOf("","","","","")
    userWord.forEachIndexed { index, s ->
        if (userWord[index] == secretWord[index]) {
            answer.set(index, "$green $s $noColor")
            secretWordList.remove(s.toString()) }
    }
    userWord.forEachIndexed { index, s ->
        if (userWord[index] == secretWord[index]) {
            answer.set(index, "$green $s $noColor")
        }
        else if (s.toString() in secretWordList ) {
            secretWordList.remove(s.toString())
            answer.set(index, "$yellow $s $noColor")
        }
        else answer.set(index, "$gray $s $noColor")
    }
    return answer
}

/**
 * IMPRIMIR HISTORIAL - Imprime todas las letras de la lista "answers", cada 5 letras hay un salto de linea.
 * @param answers - Lista mutable de Strings en la que se van guardando todas las palabras introducidas por el usuario.
 */
fun printHistorial(answer: MutableList<String>) {
    answers += answer
    print("###################")
    answers.forEachIndexed { index, v ->
        if (index % 5 == 0) println()
        if (index %5== 0) print("# ")
        print(v)
        if (index %5 == 4)print(" #") }
    println()
    println("###################")
}