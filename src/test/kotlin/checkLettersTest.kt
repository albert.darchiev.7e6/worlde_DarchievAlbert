import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

internal class checkLettersTest{
    val green = "\u001B[42m"
    val gray = "\u001B[47m"
    val yellow = "\u001B[43m"
    val noColor = "\u001b[0m"

    @Test
    fun sameLetter(){

        val result = checkLetters("ARAÑA", "AAAAA")
        val expected = mutableListOf<String>("$green A $noColor", "$gray A $noColor","$green A $noColor","$gray A $noColor","$green A $noColor")
        assertEquals(expected, result)
    }

    @Test
    fun pizza(){
        val result = checkLetters("PIZZA", "ZZXXZ")
        val expected = mutableListOf<String>("$yellow Z $noColor", "$yellow Z $noColor","$gray X $noColor","$gray X $noColor","$gray Z $noColor")
        assertEquals(expected, result)
    }
    @Test
    fun pizzav2(){
        val result = checkLetters("PIZZA", "ZZZZZ")
        val expected = mutableListOf<String>("$gray Z $noColor", "$gray Z $noColor","$green Z $noColor","$green Z $noColor","$gray Z $noColor")
        assertEquals(expected, result)
    }

    @Test
    fun correctWord(){
        val result = checkLetters("PIZZA", "PIZZA")
        val expected = mutableListOf<String>("$green P $noColor", "$green I $noColor","$green Z $noColor","$green Z $noColor","$green A $noColor")
        assertEquals(expected, result)
    }

}
