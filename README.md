# PROYECTO WORDLE

## DESCRIPCIÓN PROYECTO
    Wordle es un juego de palabras/rompecabezas. 
    Los jugadores tienen que adivinar una palabra de cinco letras, con pistas dadas con cada intento, en un maxmo de seis intentos.
![httpsLink](images_readme/wordle1.JPG)
![httpsLink](images_readme/wordle2.JPG)

## INSTALACIÓN
###  COPIAR ENLACE
     1. Acceder a https://gitlab.com/albert.darchiev.7e6/worlde_DarchievAlbert
     2. Copiar enlace dentro de Clone > Clone with HTTPS
![httpsLink](images_readme/httpsLink.JPG)

###  IMPORTAR EN INTELLIJ
     1. Acceder al menu inicial de Intellij
![importar](images_readme/cap3.JPG)

     2. Acceder a Get From VCS en la parte superior derecha
![importar](images_readme/cap2.JPG)

     3. Una vez dentro pegar el enlace HTTPS en URL
![importar](images_readme/cap4.JPG)

### ACCEDER AL JUEGO/PROYECTO Y EJECUTAR
     1. Una vez importado y dentro del proyecto acceder a:
        \Wordle\src\main\kotlin\Main.kt

    2. Despues de acceder a "Main.kt" usar combinación de teclas [CONTROL + MAYUS + F10] para ejecutar el programa
![ejecutar](images_readme/cap5.JPG)

    3. Para terminar la ejecución pulsar [CONTROL + F2]


## INSTRUCCIONES DE JUEGO
    ### OBJETIVO
    -El objetivo del juego es adivinar una palabra de cinco letras.
    -Se proporcionan pistas con cada intento.
    -Hay un maxmo de seis intentos.

    ### PISTAS
    -Pista GRIS: La palabra no se encuentra en la palabra secreta.
![pista](images_readme/cap6.JPG)

    -Pista AMARILLA: La letra se encuentra en la palabra secreta, pero esta en la posicion equivocada.
![pista](images_readme/cap7.JPG)

    -Pista VERDE: La letra se encuentra en la posición correcta. 
![pista](images_readme/cap8.JPG)
![pista](images_readme/cap9.JPG)

### SEGUIR JUGANDO
    -Una vez finalizada la partida para jugar otra partida hay que escribir "SI", en caso contrario "NO"
![pista](images_readme/cap.JPG)

## LICÉNCIA
This project is licensed under the terms of the GNU GPLv3 license and is available for free.



